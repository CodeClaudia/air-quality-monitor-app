//
//  Station.swift
//  AirQualityMonitorApp
//
//  Created by klaudia on 04.01.2018.
//  Copyright © 2018 ClaudiaCode. All rights reserved.
//

import Foundation

struct Station: Encodable {
    let id: Int
    let stationName: String
    let latitude: String
    let longitude: String
    let city: City
    let address: String
    
    enum CodingKeys: String, CodingKey {
        case latitude = "gegrLat"
        case longitude = "gegrLon"
        case address = "addressStreet"
    }
}

struct City: Encodable {
    let id: Int
    let name: String
}
